# Generated by Django 3.0.8 on 2020-07-16 16:10

from django.db import migrations, models
import django_quill.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20200716_1559'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuillPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', django_quill.fields.QuillField()),
            ],
        ),
        migrations.AlterModelOptions(
            name='post',
            options={},
        ),
    ]
