# Generated by Django 3.0.8 on 2020-07-16 15:59

from django.db import migrations
import django_quill.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20200716_1513'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='body',
            field=django_quill.fields.QuillField(),
        ),
        migrations.AlterField(
            model_name='post',
            name='body',
            field=django_quill.fields.QuillField(),
        ),
    ]
